<?php

namespace App\Http\Controllers;

use App\Models\OttuPayment;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class OttuPaymentController extends Controller
{
    public function ottuCallbackAPI(Request $request)
    {
        $payment = new OttuPayment;
        $payment->order_ref=$request->order_no;
        $payment->customer_id=$request->extra['customer_id'];
        $payment->ottu_result=$request->result;
        $payment->gateway_response=json_encode($request->all());
        $payment->save();

        return redirect()->route('paymentResult');//->with(['paymentStatus'=>$request->result]);
        //return response('captured', 200);
        //return response()->json (['message'=>'data captured successfully'], 200);
    }

    public function sendRequestToOttu(Request $request){
        $ottu_endpoint_url ='https://pay.az808080.com/pos/crt/';
        $client = new Client(['base_uri' => $ottu_endpoint_url]);

        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ];

        $info =[
            'headers' => $headers,
            'json' => [
                'amount' => $request->amount,
                'currency_code' => $request->currency_code,
                'gateway_code' => 'kpayt',
                'order_no' => $request->order_no,
                'customer_email' => $request->customer_email,
                'disclosure_url' => env('APP_URL') . 'api/ottuPaymentCallback',
                'redirect_url' => env('APP_URL') . 'disclose_ok/',
                'extra'=>[
                    'customer_id'=>1,
                    'product_id'=>1,
                    'sales_person_id'=>1,
                    'promotion_code'=>'test1',
                ],
            ]
        ];
        $response = $client->request('POST', $ottu_endpoint_url, $info);
        $paymentURL= json_decode($response->getBody());
        return redirect($paymentURL->url);
    }
}
