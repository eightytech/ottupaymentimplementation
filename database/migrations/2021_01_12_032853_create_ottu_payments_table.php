<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOttuPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ottu_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_ref',25);
            $table->integer('customer_id');
            $table->json('gateway_response');
            $table->string('ottu_result');
            $table->boolean('status')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ottu_payments');
    }
}
