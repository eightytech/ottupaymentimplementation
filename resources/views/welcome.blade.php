<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

        <title>Ottu Payment Gateway Testing</title>
    </head>
    <body>
        <form method="post" id="PaymentOrderForm" action="/sendRequestToOttu">
            @csrf
            <div class="container">
                </br>
                </br>
                <p><h3>Test Payment Here</h3></p>
                </br>
                <div class="mb-3">
                    <label for="amount" class="form-label">Payment Amount</label>
                    <input type="text" class="form-control" id="AmountText" name="amount" aria-describedby="amount" value="15.000">
                    <div id="amount" class="form-text">must be positive number</div>
                </div>

                <div class="mb-3">
                    <label for="currency" class="form-label">Amount Currency</label>
                    <input class="form-control" type="text" id="currency" name="currency_code" value="KWD" aria-label="Disabled input example">
                </div>

                <div class="mb-3">
                    <label for="OrderNumber" class="form-label">Order Number</label>
                    <input type="text" class="form-control" id="OrderNumberText" name="order_no" aria-describedby="orderNumber" value="{{uniqid('en')}}">
                    <div id="orderNumber" class="form-text">must be unique</div>
                </div>


                <div class="mb-3">
                    <label for="EmailInput" class="form-label">Email address</label>
                    <input type="email" class="form-control" id="EmailInput1" name="customer_email" value="name@example.com">
                    <div id="EmailInput" class="form-text">must be unique</div>
                </div>

                <button type="submit" class="btn btn-primary">Go To Payment</button>

                <script>

                    function goToPayment() {

                    }

                </script>
            </div>
        </form>
    </body>
</html>
