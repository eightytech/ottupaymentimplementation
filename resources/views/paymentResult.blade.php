<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <title>Ottu Payment Gateway Testing - Payment Result</title>
</head>
<body>
<div class="container">
    <div class="card" style="width: 22rem;">
        <div class="card-body">
            <h5 class="card-title">Payment Status</h5>
            <h6 class="card-subtitle mb-2 text-muted">Thank you for your purchase</h6>
            <p class="card-text">Your payment status is:
            @if (isset($paymentStatus) && $paymentStatus === 'success')
                <div class="alert alert-success" role="alert">
                    {{ $paymentStatus }}
                </div>
            @elseif(!isset($paymentStatus) || $paymentStatus != 'success')
                <div class="alert alert-danger" role="alert">
                    {{ isset($paymentStatus) ? $paymentStatus : 'still pending'}}
                </div>
            @endif
            </p>
            <a href="#" class="card-link">Home</a>
            <a href="#" class="card-link">Contact Us</a>
        </div>
    </div>
</div>
</body>
</html>
